window.onload = function(){

    
    var Descriptions = {
        currentDescription: null,
        description: document.getElementById("description"),
        play: document.getElementById("PlayDesc"),
        network: document.getElementById("NetworkDesc"),
        controls: document.getElementById("ControlsDesc"),
        history: document.getElementById("HistoryDesc"),
        configure: document.getElementById("ConfigureDesc"),
        credits: document.getElementById("CreditsDesc")
    }
    var Buttons = {
        
        play: document.getElementById("PlayBtn"),
        network: document.getElementById("NetworkBtn"),
        controls: document.getElementById("ControlsBtn"),
        history: document.getElementById("HistoryBtn"),
        configure: document.getElementById("ConfigureBtn"),
        credits: document.getElementById("CreditsBtn")
    }
    Descriptions.description.addEventListener('webkitAnimationEnd', function(){
        this.style.webkitAnimationName = '';
        Descriptions.currentDescription.style.display = "block";
        }, false);
    Buttons.play.onclick = function(){
        Descriptions.description.style.webkitAnimation = "";
        Descriptions.currentDescription.style.display = "none";
        Descriptions.description.style.webkitAnimation =  "flip 1000ms ease-in";
        Descriptions.currentDescription = Descriptions.play;
    }
    
    Buttons.network.onclick = function(){
        Descriptions.description.style.webkitAnimation = "";
        Descriptions.currentDescription.style.display = "none";
        Descriptions.description.style.webkitAnimation =  "flip 1000ms ease-in";
        Descriptions.currentDescription = Descriptions.network;
    }
    
    Buttons.controls.onclick = function(){
        Descriptions.description.style.webkitAnimation = "";
        Descriptions.currentDescription.style.display = "none";
        Descriptions.description.style.webkitAnimation =  "flip 1000ms ease-in";
        Descriptions.currentDescription = Descriptions.controls;
    }
    
    Buttons.history.onclick = function(){
        Descriptions.description.style.webkitAnimation = "";
        Descriptions.currentDescription.style.display = "none";
        Descriptions.description.style.webkitAnimation =  "flip 1000ms ease-in";
        Descriptions.currentDescription = Descriptions.history;
    }
    
    Buttons.configure.onclick = function(){
        Descriptions.description.style.webkitAnimation = "";
        Descriptions.currentDescription.style.display = "none";
        Descriptions.description.style.webkitAnimation =  "flip 1000ms ease-in";
        Descriptions.currentDescription = Descriptions.configure;
    }
    
    Buttons.credits.onclick = function(){
        Descriptions.description.style.webkitAnimation = "";
        Descriptions.currentDescription.style.display = "none";
        Descriptions.description.style.webkitAnimation =  "flip 1000ms ease-in";
        Descriptions.currentDescription = Descriptions.credits;
    }
    
    Descriptions.currentDescription = Descriptions.play;

}
